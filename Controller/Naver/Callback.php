<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-04-05
 * Time: 오후 4:50
 */

namespace Eguana\SocialLogin\Controller\Naver;

use Eguana\SocialLogin\Helper\Naver\Data;
use Eguana\SocialLogin\Model\CreateCustomer;
use Eguana\SocialLogin\Model\CallApi;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class Callback. Bring customer profile from naver
 */
class Callback extends Action
{

    const NAVER_TYPE = 'naver';

    protected $_messageManager;

    protected $_createCustomer;

    protected $_helper;
    /**
     * @var CallApi
     */
    private $callApi;

    /**
     * Callback constructor.
     * @param Context $context
     * @param ManagerInterface $messageManager
     * @param CreateCustomer $createCustomer
     * @param CallApi $callApi
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        CreateCustomer $createCustomer,
        CallApi $callApi,
        Data $helper
    ) {
        $this->_messageManager = $messageManager;
        $this->_createCustomer = $createCustomer;
        $this->_helper = $helper;
        $this->callApi = $callApi;
        parent::__construct($context);
    }

    public function execute()
    {
        $tokenApiUrl = $this->_helper->getTokenApiUrl();
        $client_id = $this->_helper->getClientId();
        $client_secret = $this->_helper->getClientSecret();
        $redirectURI = urlencode($this->_helper->getCallbackUrl());
        $profileUrl = $this->_helper->getProfileApiUrl();
        $code = $this->getRequest()->getParam('code');
        $state = $this->getRequest()->getParam('state');

        $token_url = $this->createTokenUrl($tokenApiUrl, $client_id, $client_secret, $redirectURI, $code, $state);

        $access_token = $this->callApi->getAccessToken($token_url);
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($access_token !='') {
            $profile = $this->callApi->getProfile($profileUrl, $access_token);

            if (!empty($profile['response'])) {
                $result = $this->_createCustomer->createCustomer($profile['response'], self::NAVER_TYPE);

                if ($result !== 'success') {
                    $this->messageManager->addErrorMessage($result);
                }
                $resultRedirect->setPath('customer/account');
            } else {
                $message = $this->_createCustomer->getErrorMessage('Profile', self::NAVER_TYPE);
                $this->messageManager->addErrorMessage($message);
            }
        } else {
            $message = $this->_createCustomer->getErrorMessage('Token', self::NAVER_TYPE);
            $this->messageManager->addErrorMessage($message);
        }

        return $resultRedirect;
    }

    /**
     * Create naver token api url.
     *
     * @param $tokenUrl
     * @param $client_id
     * @param $client_secret
     * @param $redirectURI
     * @param $code
     * @param $state
     * @return string
     */
    private function createTokenUrl($tokenUrl, $client_id, $client_secret, $redirectURI, $code, $state)
    {
        $url = $tokenUrl."?grant_type=authorization_code&client_id=".$client_id."&client_secret=".$client_secret."&redirect_uri=".$redirectURI."&code=".$code."&state=".$state;

        return $url;
    }

}