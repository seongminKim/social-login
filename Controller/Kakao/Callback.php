<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-04-11
 * Time: 오전 9:14
 */

namespace Eguana\SocialLogin\Controller\Kakao;

use Eguana\SocialLogin\Helper\Kakao\Data;
use Eguana\SocialLogin\Model\CreateCustomer;
use Eguana\SocialLogin\Model\CallApi;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class Callback. Bring customer profile from kakao
 */
class Callback extends Action
{

    const KAKAO_TYEP = 'Kakao';

    protected $_messageManager;

    protected $_createCustomer;

    protected $_helper;
    /**
     * @var CallApi
     */
    private $callApi;

    /**
     * Callback constructor.
     * @param Context $context
     * @param ManagerInterface $messageManager
     * @param CreateCustomer $createCustomer
     * @param CallApi $callApi
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        CreateCustomer $createCustomer,
        CallApi $callApi,
        Data $helper
    ) {
        $this->_messageManager = $messageManager;
        $this->_createCustomer = $createCustomer;
        $this->_helper = $helper;
        $this->callApi = $callApi;
        parent::__construct($context);
    }

    public function execute()
    {
        $tokenApiUrl = $this->_helper->getTokenApiUrl();
        $code = $this->_request->getParam('code');
        $api_key = $this->_helper->getApiKey();
        $callbackUrl = urlencode($this->_helper->getCallbackUrl());
        $profileUrl = $this->_helper->getProfileApiUrl();

        $token_url = $this->createTokenUrl($tokenApiUrl, $api_key, $callbackUrl, $code);

        $access_token = $this->callApi->getAccessToken($token_url);
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($access_token !='') {
            $profileData = $this->callApi->getProfile($profileUrl, $access_token);

            if (!empty($profileData)) {
                $profile = $this->cleanProfileData($profileData);
                $result = $this->_createCustomer->createCustomer($profile, self::KAKAO_TYEP);

                if ($result !== 'success') {
                    $this->messageManager->addErrorMessage($result);
                }
                $resultRedirect->setPath('customer/account');
            } else {
                $message = $this->_createCustomer->getErrorMessage('Profile', self::KAKAO_TYEP);
                $this->messageManager->addErrorMessage($message);
            }
        } else {
            $message = $this->_createCustomer->getErrorMessage('Token', self::KAKAO_TYEP);
            $this->messageManager->addErrorMessage($message);
        }

        return $resultRedirect;
    }

    /**
     * Create kakao token api url.
     *
     * @param $tokenApiUrl
     * @param $api_key
     * @param $callbackUrl
     * @param $code
     * @return string
     */
    private function createTokenUrl($tokenApiUrl, $api_key, $callbackUrl, $code)
    {
        $url = $tokenApiUrl."?grant_type=authorization_code&client_id=".$api_key."&redirect_uri=".$callbackUrl."&code=".$code;

        return $url;
    }


    /**
     * Save profile data array into new array
     * @param $profileData
     * @return mixed
     */
    private function cleanProfileData($profileData)
    {
        $name = (!empty($profileData['properties']['nickname'])) ? $profileData['properties']['nickname'] : [];
        $email = (!empty($profileData['kakao_account']['email'])) ? $profileData['kakao_account']['email'] : [];

        $profile['name'] = $name;
        $profile['email'] = $email;

        return $profile;
    }
}