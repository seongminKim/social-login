<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-09-30
 * Time: 오후 4:16
 */

namespace Eguana\SocialLogin\Controller\Google;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Eguana\SocialLogin\Helper\Google\Data;
use Eguana\SocialLogin\Model\CreateCustomer;
use Eguana\SocialLogin\Model\CallApi;
use Magento\Framework\Message\ManagerInterface;

class Callback extends Action
{
    const GOOGLE_TYPE = 'Google';

    /**
     * @var CreateCustomer
     */
    private $createCustomer;

    protected $messageManager;
    /**
     * @var CallApi
     */
    private $callApi;
    /**
     * @var Data
     */
    private $helper;

    /**
     * Callback constructor.
     * @param Context $context
     * @param ManagerInterface $messageManager
     * @param CreateCustomer $createCustomer
     * @param CallApi $callApi
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        CreateCustomer $createCustomer,
        CallApi $callApi,
        Data $helper
    ) {
        parent::__construct($context);
        $this->messageManager = $messageManager;
        $this->createCustomer = $createCustomer;
        $this->callApi = $callApi;
        $this->helper = $helper;
    }

    public function execute()
    {
        $tokenApiUrl = $this->helper->getGoogleTokenUrl();
        $client_id = $this->helper->getGoogleClientId();
        $redirectURI = $this->helper->getGoogleCallbackUrl();
        $client_secret = $this->helper->getGoogleClientSecret();
        $code = $this->_request->getParam('code');

        $token_url = $this->createTokenUrl($tokenApiUrl, $client_id, $redirectURI, $client_secret, $code);

        $access_token = $this->getGoogleToken($token_url);
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($access_token !='') {
            $profile = $this->getGoogleProfile($access_token);

            if (!empty($profile)) {
                $result = $this->createCustomer->createCustomer($profile, self::GOOGLE_TYPE);

                if ($result !== 'success') {
                    $this->messageManager->addErrorMessage($result);
                }
                $resultRedirect->setPath('customer/account');
            } else {
                $message = $this->createCustomer->getErrorMessage('Profile', self::GOOGLE_TYPE);
                $this->messageManager->addErrorMessage($message);
            }
        } else {
            $message = $this->createCustomer->getErrorMessage('Token', self::GOOGLE_TYPE);
            $this->messageManager->addErrorMessage($message);
        }

        return $resultRedirect;
    }

    /**
     * Create facebook api token url.
     *
     * @param $tokenUrl
     * @param $client_id
     * @param $redirectURI
     * @param $client_secret
     * @param $code
     * @return string
     */
    private function createTokenUrl($tokenUrl, $client_id, $redirectURI, $client_secret, $code)
    {
        $token_url = $tokenUrl.'?client_id='.$client_id.'&redirect_uri='.$redirectURI.'&client_secret='.$client_secret.'&code='.$code.'&grant_type=authorization_code';

        return $token_url;
    }

    /**
     * Get profile from facebook api
     * @param $access_token
     * @return array
     */
    private function getGoogleProfile($access_token){

        $profileUrl = $this->helper->getGoogleProfileUrl();

        $ch_pro = curl_init();

        $pro_headers = [sprintf('Authorization: Bearer %s', $access_token)];

        curl_setopt($ch_pro, CURLOPT_URL, $profileUrl);
        curl_setopt($ch_pro, CURLOPT_POST, false);
        curl_setopt($ch_pro, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_pro, CURLOPT_HTTPHEADER, $pro_headers);

        $pro_response = curl_exec ($ch_pro);
        $status_code = curl_getinfo($ch_pro, CURLINFO_HTTP_CODE);

        curl_close ($ch_pro);

        $profile = [];

        if($status_code == 200) {
            $profile = json_decode($pro_response,true);
        }
        return $profile;
    }

    /**
     * Get token for google login service
     *
     * @param $token_url
     * @return string
     */
    protected function getGoogleToken($token_url)
    {
        $ch = curl_init();

        $is_post = true;
        $pro_headers = ['Content-Type: application/x-www-form-urlencoded', 'Content-length: 0'];

        curl_setopt($ch, CURLOPT_URL, $token_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $pro_headers);
        curl_setopt($ch, CURLOPT_POST, $is_post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close ($ch);

        $access_token='';

        if($status_code == 200) {
            $responseArr = json_decode($response, true);
            $access_token = $responseArr['access_token'];
        }

        return $access_token;
    }
}