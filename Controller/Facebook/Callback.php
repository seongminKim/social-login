<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-04-05
 * Time: 오후 4:50
 */

namespace Eguana\SocialLogin\Controller\Facebook;

use Eguana\SocialLogin\Helper\Facebook\Data;
use Eguana\SocialLogin\Model\CreateCustomer;
use Eguana\SocialLogin\Model\CallApi;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class Callback. Bring customer profile from facebook
 */
class Callback extends Action
{
    const FACEBOOK_TYPE = 'facebook';

    protected $_messageManager;

    protected $_createCustomer;

    protected $_facebookHelper;
    /**
     * @var CallApi
     */
    private $callApi;

    /**
     * Callback constructor.
     * @param Context $context
     * @param ManagerInterface $messageManager
     * @param CreateCustomer $createCustomer
     * @param CallApi $callApi
     * @param Data $facebookHelper
     */
    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        CreateCustomer $createCustomer,
        CallApi $callApi,
        Data $facebookHelper
    ) {
        $this->_messageManager = $messageManager;
        $this->_createCustomer = $createCustomer;
        $this->_facebookHelper = $facebookHelper;
        $this->callApi = $callApi;
        parent::__construct($context);
    }

    public function execute()
    {
        $tokenApiUrl = $this->_facebookHelper->getTokenApiUrl();
        $client_id = $this->_facebookHelper->getAppId();
        $redirectURI = $this->_facebookHelper->getCallbackUrl();
        $client_secret = $this->_facebookHelper->getAppSecret();
        $code = $this->_request->getParam('code');

        $token_url = $this->createTokenUrl($tokenApiUrl, $client_id, $redirectURI, $client_secret, $code);

        $access_token = $this->callApi->getAccessToken($token_url);
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($access_token !='') {
            $profile = $this->getFacebookProfile($access_token);

            if (!empty($profile)) {
                $result = $this->_createCustomer->createCustomer($profile, self::FACEBOOK_TYPE);

                if ($result !== 'success') {
                    $this->messageManager->addErrorMessage($result);
                }
                $resultRedirect->setPath('customer/account');
            } else {
                $message = $this->_createCustomer->getErrorMessage('Profile', self::FACEBOOK_TYPE);
                $this->messageManager->addErrorMessage($message);
            }
        } else {
            $message = $this->_createCustomer->getErrorMessage('Token', self::FACEBOOK_TYPE);
            $this->messageManager->addErrorMessage($message);
        }

        return $resultRedirect;
    }

    /**
     * Create facebook api token url.
     *
     * @param $tokenUrl
     * @param $client_id
     * @param $redirectURI
     * @param $client_secret
     * @param $code
     * @return string
     */
    private function createTokenUrl($tokenUrl, $client_id, $redirectURI, $client_secret, $code)
    {
        $token_url = $tokenUrl.'?client_id='.$client_id.'&redirect_uri='.$redirectURI.'&client_secret='.$client_secret.'&code='.$code;

        return $token_url;
    }

    /**
     * Get profile from facebook api
     * @param $access_token
     * @return array
     */
    private function getFacebookProfile($access_token){

        $profileUrl = $this->_facebookHelper->getProfileApiUrl();
        $pro_url = $profileUrl.'?access_token='.$access_token.'&fields=id,name,email';

        $ch_pro = curl_init();

        curl_setopt($ch_pro, CURLOPT_URL, $pro_url);
        curl_setopt($ch_pro, CURLOPT_POST, false);
        curl_setopt($ch_pro, CURLOPT_RETURNTRANSFER, true);

        $pro_response = curl_exec ($ch_pro);
        $status_code = curl_getinfo($ch_pro, CURLINFO_HTTP_CODE);

        curl_close ($ch_pro);

        $profile = [];

        if($status_code == 200) {
            $profile = json_decode($pro_response,true);
        }
        return $profile;
    }
}