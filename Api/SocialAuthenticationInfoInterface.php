<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-09-27
 * Time: 오후 1:43
 */

namespace Eguana\SocialLogin\Api;

/**
 * Interface SocialAuthenticationInfoInterface
 * @package Eguana\SocialLogin\Api
 */

interface SocialAuthenticationInfoInterface
{
    /**
     * @param $token_url
     * @return mixed
     */
    public function getAccessToken($token_url);

    /**
     * @param $profileUrl
     * @param $access_token
     * @return mixed
     */
    public function getProfile($profileUrl, $access_token);
}