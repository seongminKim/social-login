<?php

namespace Eguana\SocialLogin\Block\Form;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class Login extends \Magento\Customer\Block\Form\Login
{
    /**
     * @var \Eguana\SocialLogin\Helper\Naver\Data
     */
    private $_naverHelper;

    /**
     * @var \Eguana\SocialLogin\Helper\Facebook\Data
     */
    private $_facebookHelper;

    /**
     * @var \Eguana\SocialLogin\Helper\Kakao\Data
     */
    private $_kakaoHelper;

    /**
     * @var \Eguana\SocialLogin\Helper\Google\Data
     */
    private $googleHelper;

    protected $_storeManager;

    /**
     * Login constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param Url $customerUrl
     * @param \Eguana\SocialLogin\Helper\Naver\Data $naverHelper
     * @param \Eguana\SocialLogin\Helper\Facebook\Data $facebookHelper
     * @param \Eguana\SocialLogin\Helper\Kakao\Data $kakaoHelper
     * @param \Eguana\SocialLogin\Helper\Google\Data $googleHelper
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Url $customerUrl,
        \Eguana\SocialLogin\Helper\Naver\Data $naverHelper,
        \Eguana\SocialLogin\Helper\Facebook\Data $facebookHelper,
        \Eguana\SocialLogin\Helper\Kakao\Data $kakaoHelper,
        \Eguana\SocialLogin\Helper\Google\Data $googleHelper,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->_naverHelper = $naverHelper;
        $this->_facebookHelper = $facebookHelper;
        $this->_kakaoHelper = $kakaoHelper;
        $this->_storeManager = $storeManager;
        $this->googleHelper = $googleHelper;
        parent::__construct($context, $customerSession, $customerUrl, $data);
    }

    public function naverLogin()
    {
        $enable = $this->_naverHelper->getNaverLoginEnabled();
        $loginUrl = $this->_naverHelper->getLoginApiUrl();
        $client_id = $this->_naverHelper->getClientId();
        $redirectUrl = urlencode($this->_naverHelper->getCallbackUrl());
        $state = md5(microtime().mt_rand());

        $url = $loginUrl."?response_type=code&client_id=".$client_id."&redirect_uri=".$redirectUrl."&state=".$state;

        $data['enable'] = $enable;
        $data['url'] = $url;

        return $data;
    }

    public function facebookLogin()
    {
        $enable = $this->_facebookHelper->getFacebookLoginEnabled();
        $loginUrl = $this->_facebookHelper->getLoginApiUrl();
        $client_id = $this->_facebookHelper->getAppId();
        $redirectUrl = $this->_facebookHelper->getCallbackUrl();
        $state = md5(microtime().mt_rand());

        $url = $loginUrl.'?client_id='.$client_id.'&redirect_uri='.$redirectUrl.'&state='.$state.'&scope=public_profile,email';

        $data['enable'] = $enable;
        $data['url'] = $url;

        return $data;
    }

    public function kakaoLogin()
    {
        $enable = $this->_kakaoHelper->getKakaoLoginEnabled();
        $loginUrl = $this->_kakaoHelper->getLoginApiUrl();
        $api_key = $this->_kakaoHelper->getApiKey();
        $callbackUrl = urlencode($this->_kakaoHelper->getCallbackUrl());

        $url = $loginUrl."?client_id=".$api_key."&redirect_uri=".$callbackUrl."&response_type=code";

        $data['enable'] = $enable;
        $data['url'] = $url;

        return $data;
    }

    public function googleLogin()
    {
        $enable = $this->googleHelper->getGoogleLoginEnabled();
        $client_id = $this->googleHelper->getGoogleClientId();
        $redirectUrl = $this->googleHelper->getGoogleCallbackUrl();
        $loginUrl = $this->googleHelper->getGoogleLoginUrl();
        $state='OK';
        $access_type='offline';
        $include_granted_scopes = 'true';

        $url = $loginUrl.'?client_id='.$client_id.'&redirect_uri='.$redirectUrl.'&state='.$state.'&access_type='.$access_type.'&include_granted_scopes='.$include_granted_scopes.'&response_type=code'.'&scope=email';

        $data['enable'] = $enable;
        $data['url'] = $url;

        return $data;
    }

}
