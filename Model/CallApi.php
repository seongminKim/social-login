<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-09-26
 * Time: 오후 1:46
 */

namespace Eguana\SocialLogin\Model;

use Eguana\SocialLogin\Api\SocialAuthenticationInfoInterface;

class CallApi implements SocialAuthenticationInfoInterface
{
    /**
     * Get token for social login service
     *
     * @param $token_url
     * @return string
     */
    public function getAccessToken($token_url)
    {
        $is_post = true;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $token_url);
        curl_setopt($ch, CURLOPT_POST, $is_post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close ($ch);

        $access_token='';

        if($status_code == 200) {
            $responseArr = json_decode($response, true);
            $access_token = $responseArr['access_token'];
        }

        return $access_token;
    }

    /**
     * Get customer profile(email, name)
     *
     * @param $profileUrl
     * @param $access_token
     * @return array
     */
    public function getProfile($profileUrl, $access_token)
    {
        $pro_ch = curl_init();

        $pro_is_post = false;
        $pro_headers = ['Content-Type: application/json', sprintf('Authorization: Bearer %s', $access_token)];

        curl_setopt($pro_ch, CURLOPT_URL, $profileUrl);
        curl_setopt($pro_ch, CURLOPT_POST, $pro_is_post);
        curl_setopt($pro_ch, CURLOPT_HTTPHEADER, $pro_headers);
        curl_setopt($pro_ch, CURLOPT_RETURNTRANSFER, true);

        $pro_response = curl_exec ($pro_ch);
        $pro_status_code = curl_getinfo($pro_ch, CURLINFO_HTTP_CODE);

        curl_close ($pro_ch);

        $profile = [];

        if($pro_status_code == 200) {
            $profile = json_decode($pro_response,true);
        }

        return $profile;
    }
}