<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-04-09
 * Time: 오후 2:40
 */

namespace Eguana\SocialLogin\Model;


use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Math\Random;
use Magento\Store\Model\StoreManagerInterface;

class CreateCustomer
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    protected $mathRandom;

    protected $_customerSession;

    /**
     * @param StoreManagerInterface $storeManager
     * @param CustomerFactory $customerFactory
     * @param Random $mathRandom
     * @param Session $customerSession
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
        Random $mathRandom,
        Session $customerSession
    ) {
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->mathRandom = $mathRandom;
        $this->_customerSession = $customerSession;

    }

    public function createCustomer($profile, $type)
    {
        if (!empty($profile['email'])) {
            $email = $profile['email'];
        } else {
            $message = $this->getErrorMessage('Email', $type);
            return $message;
        }

        if (!empty($profile['name'])) {
            $name = $profile['name'];
        } else {
            $message = $this->getErrorMessage('Name', $type);
            return $message;
        }

        $id_eng_check = $this->customerNameEngCheck($name);

        if ($id_eng_check == 1) {
            $firstName  = iconv_substr($name,1,10,"utf-8");
            $lastName = iconv_substr($name,0,1,"utf-8");
        }else{
            $firstName = $name;
            $lastName = $name;
        }

        try {
            $customerFactory = $this->customerFactory->create();
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
            $customerFactory->setWebsiteId($websiteId);
            $customer = $customerFactory->loadByEmail($email);

            if ($customer->getId()) {
                $this->_customerSession->setCustomerAsLoggedIn($customer);
            }else{
                $customer->setEmail($email);
                $customer->setFirstname($firstName);
                $customer->setLastname($lastName);
                $password = $this->generatePassword();
                $customer->setPassword($password);
                $customer->setGroupId(1);

                $customer->save();

                $this->_customerSession->setCustomerAsLoggedIn($customer);
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            return $message;
        }
        return $message = 'success';
    }

    private function generatePassword($length = 10)
    {
        $chars = Random::CHARS_LOWERS
            . Random::CHARS_UPPERS
            . Random::CHARS_DIGITS;

        return $password = $this->mathRandom->getRandomString($length, $chars);
    }

    private function customerNameEngCheck($name)
    {
        $id_eng_check = preg_match("/[\xA1-\xFE][\xA1-\xFE]/", $name);

        return $id_eng_check;
    }

    public function getErrorMessage($text, $type)
    {
        $message = 'Error during social login process';

        if ($text == 'Profile') {
            $message = __('Fail to bring '. $text. ' from '. $type);
        } elseif ($text == 'Token') {
            $message = __('Fail to bring '. $text. ' from '. $type);
        } elseif ($text == 'Email') {
            $message = __($text. ' must provide to our website from '. $type);
        } elseif ($text == 'Name') {
            $message = __($text. ' must provide to our website from '. $type);
        }

        return $message;
    }
}