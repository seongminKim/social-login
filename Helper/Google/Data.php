<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-09-30
 * Time: 오후 2:40
 */

namespace Eguana\SocialLogin\Helper\Google;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getGoogleLoginEnabled(){
        return $this->scopeConfig->getValue('soical_login/google_login/enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleClientId(){
        return $this->scopeConfig->getValue('soical_login/google_login/google_client_id', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleClientSecret()
    {
        return $this->scopeConfig->getValue('soical_login/google_login/google_client_secret', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleLoginUrl()
    {
        return $this->scopeConfig->getValue('soical_login/google_login/google_login_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleTokenUrl()
    {
        return $this->scopeConfig->getValue('soical_login/google_login/google_token_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleProfileUrl()
    {
        return $this->scopeConfig->getValue('soical_login/google_login/google_profile_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleCallbackUrl()
    {
        return $this->scopeConfig->getValue('soical_login/google_login/google_callback_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getGoogleScopekUrl()
    {
        return $this->scopeConfig->getValue('soical_login/google_login/google_scope_url', ScopeInterface::SCOPE_STORE);
    }
}