<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-04-09
 * Time: 오후 3:39
 */

namespace Eguana\SocialLogin\Helper\Kakao;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getKakaoLoginEnabled(){
        return $this->scopeConfig->getValue('soical_login/kakao_login/enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getLoginApiUrl(){
        return $this->scopeConfig->getValue('soical_login/kakao_login/kakao_login_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getTokenApiUrl()
    {
        return $this->scopeConfig->getValue('soical_login/kakao_login/kakao_token_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getProfileApiUrl()
    {
        return $this->scopeConfig->getValue('soical_login/kakao_login/kakao_profile_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->scopeConfig->getValue('soical_login/kakao_login/kakao_callback_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue('soical_login/kakao_login/kakao_api_key', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getEmailSite()
    {
        return $this->scopeConfig->getValue('soical_login/kakao_login/email_site', ScopeInterface::SCOPE_STORE);
    }

}