<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-04-09
 * Time: 오후 3:39
 */

namespace Eguana\SocialLogin\Helper\Naver;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getNaverLoginEnabled(){
        return $this->scopeConfig->getValue('soical_login/naver_login/enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getLoginApiUrl(){
        return $this->scopeConfig->getValue('soical_login/naver_login/naver_login_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getTokenApiUrl()
    {
        return $this->scopeConfig->getValue('soical_login/naver_login/naver_token_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getProfileApiUrl()
    {
        return $this->scopeConfig->getValue('soical_login/naver_login/naver_profile_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->scopeConfig->getValue('soical_login/naver_login/naver_callback_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->scopeConfig->getValue('soical_login/naver_login/naver_client_id', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->scopeConfig->getValue('soical_login/naver_login/naver_client_secret', ScopeInterface::SCOPE_STORE);
    }

}